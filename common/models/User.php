<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string photo
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Статус',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'photo' => 'Фотография пользователя',
            'pay_metod' => 'Метод оплаты по умолчанию',
            'pay_phone' => 'Телефон',
            'pay_yandex' => 'Яндекс.Деньги',
            'pay_qiwi' => 'Киви.Кошелек',
        ];
    }

    public static function tableName()
    {
        return '{{%user}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'required'],
            [['id', 'status'], 'integer'],
            [['first_name', 'last_name', 'pay_metod', 'pay_phone', 'pay_yandex', 'pay_qiwi'], 'string', 'min' => 3, 'max' => 255],
            ['email', 'email'],
            [['email'], 'unique'],
            ['email', 'default', 'value' => NULL],
            ['pay_metod', 'default', 'value' => 'pay_vk'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['photo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    public static function getAll()
    {
        return self::find()->all();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @param $id
     * @param $first_name
     * @param $last_name
     * @param $photo
     * @return bool
     * @throws \yii\base\Exception
     * @throws \Exception
     */
    public function saveFromVk($id, $first_name, $last_name, $photo)
    {
        $user = self::findOne($id);

        if($user) {
            return Yii::$app->user->login($user);
        }
        $this->id = $id;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->photo = $photo;
        $this->generateAuthKey();

        $this->save(false);

        if ($this->save(false))
        {
            $auth = Yii::$app->authManager;
            $role = $auth->getRole('user');
            $auth->assign($role, $this->id);

            return Yii::$app->user->login($this);
        }

        return false;
    }

    /**
     * @param $file
     * @param $currentImage
     * @return string
     * @throws \yii\base\Exception
     */
    public function uploadFile($file, $currentImage)
    {
        $this->photo = $file;

        $imageName = 'ava_' . time( ) . '_' . mt_rand(100000, 1000000) . '.jpg';
        $pathImage = Yii::getAlias('@frontend') . '/web/uploads/users/';

        if (!empty($currentImage) && $currentImage !== null && file_exists($pathImage . $currentImage)) {
            unlink($pathImage . $currentImage);
        }

        FileHelper::createDirectory($pathImage);
//        VarDumper::dump($pathImage);
//        VarDumper::dump($pathImage . $this->id . '/' . $imageName);
//        die;
            $this->photo->saveAs($pathImage . $this->id . '/' . $imageName);
            return $this->id . '/' . $imageName;

    }


    public function getImage()
    {
        return '/uploads/users/' . $this->photo;

    }








    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param $first_name
     * @return static|null
     */
    public static function findByUsername($first_name)
    {
        return static::findOne(['first_name' => $first_name]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token) {
        return static::findOne([
            'verification_token' => $token,
            'status' => self::STATUS_ACTIVE
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }


    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     * @throws \yii\base\Exception
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     * @throws \yii\base\Exception
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * @throws \yii\base\Exception
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }




}
