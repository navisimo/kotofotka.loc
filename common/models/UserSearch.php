<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use yii\data\Pagination;

/**
 * UserSearch represents the model behind the search form of `common\models\User`.
 */
class UserSearch extends User
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['first_name', 'last_name', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'photo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation(реализуется) in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied -
     * Создает экземпляр поставщика данных с примененным поисковым запросом.
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //возвращает новый объект запроса, являющийся представителем класса yii\db\ActiveQuery.
        $query = User::find();

        // add conditions that should always apply here
        //добавить условия, которые всегда должны применяться здесь
        //применяется для разбивки и сортировки
        //ActiveDataProvider предоставляет данные, выполняя запросы к БД с использованием $ query.
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            //раскомментируйте следующую строку, если вы не хотите возвращать какие-либо записи при сбое проверки
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        //условия фильтрации сетки
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'photo', $this->photo]);

        return $dataProvider;
    }


    public function search_status($page_num)
    {

        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1,
                'page' => $page_num - 1,
            ],
        ]);


        return $dataProvider;
    }

}
