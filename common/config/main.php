<?php

use yii\caching\FileCache;
use kartik\datecontrol\Module;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'layout'=>'new_layout',
    'language' => 'ru-RU',
    'sourceLanguage'=>'ru-RU',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'datecontrol' =>  [
            'class' => Module::class
        ]
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.YYYY',
            'timeFormat' => 'php:H:i:s',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'defaultTimeZone' => 'UTC',
            'timeZone' => 'Europe/Moscow',
            'locale' => 'ru-RU'
        ],
        'cache' => [
            'class' => FileCache::class,
        ],
    ],
];
