<?php

use yii\db\Migration;

/**
 * Class m190930_205804_rename_repost_column_to_article_table
 */
class m190930_205804_rename_repost_column_to_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('article', 'repost', 'point');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('article', 'point', 'repost');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190930_205804_rename_repost_column_to_article_table cannot be reverted.\n";

        return false;
    }
    */
}
