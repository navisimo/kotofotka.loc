<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user}}`.
 */
class m190909_212213_add_pay_metod_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'pay_metod', $this->string(255));
        $this->addColumn('{{%user}}', 'pay_phone', $this->string(255));
        $this->addColumn('{{%user}}', 'pay_yandex', $this->string(255));
        $this->addColumn('{{%user}}', 'pay_qiwi', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'pay_metod');
        $this->dropColumn('{{%user}}', 'pay_phone');
        $this->dropColumn('{{%user}}', 'pay_yandex');
        $this->dropColumn('{{%user}}', 'pay_qiwi');
    }
}
