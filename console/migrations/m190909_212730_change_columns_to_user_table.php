<?php

use yii\db\Migration;

/**
 * Class m190909_212730_change_columns_to_user_table
 */
class m190909_212730_change_columns_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%user}}', 'password_hash', $this->string(255)->Null());
        $this->alterColumn('{{%user}}', 'email', $this->string(255)->Null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%user}}', 'password_hash', $this->string(255)->notNull());
        $this->alterColumn('{{%user}}', 'email', $this->string(255)->notNull());

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190909_212730_change_password_hash_column_to_user_table cannot be reverted.\n";

        return false;
    }
    */
}
