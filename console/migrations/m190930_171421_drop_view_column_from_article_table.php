<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%article}}`.
 */
class m190930_171421_drop_view_column_from_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%article}}', 'viewed');
        $this->dropColumn('{{%article}}', 'likes');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%article}}', 'viewed', $this->integer());
        $this->addColumn('{{%article}}', 'likes', $this->integer());
    }
}
