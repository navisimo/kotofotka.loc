<?php

use kartik\datecontrol\DateControl;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\Article;

$user = Yii::$app->user->identity;
/* @var $this yii\web\View */
/* @var $model frontend\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row article-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="col-lg-8">

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 7, 'style' => 'resize: vertical']) ?>

    </div>

    <div class="col-lg-4">

        <?php if($model->id):?>
            <?= Html::img('/uploads/users/'.$model->image, ['alt' => $model->title, 'width' => 300]) ?>
        <?php endif;?>

        <?= $form->field($model, 'image')->fileInput() ?>

        <?php if(Yii::$app->user->can('moder')):?>

            <?= $form->field($model, 'status')->dropDownList(Article::$article_statuses_map) ?>

            <?= $form->field($model, 'date')->widget(DateControl::classname(), [
                'type'=>DateControl::FORMAT_DATE,
                //'ajaxConversion'=>false,
                'saveFormat' => 'php:Y-m-d',
                'displayFormat' => 'php:d.m.Y',
                'widgetOptions' => [
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]
            ]) ?>

        <?php endif;?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Отправить на модерацию' : 'Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
    </div>



    <?php ActiveForm::end(); ?>

</div>
