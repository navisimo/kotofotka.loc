<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user->identity;
$vk_link = 'https://vk.com/club'
    . \Yii::$app->params['groupID']
    . '?w=wall-' . \Yii::$app->params['groupID']
    . '_'
    . $model->publication_id;

\yii\web\YiiAsset::register($this);
?>
<div class="article-view">

    <?php if( Yii::$app->session->hasFlash('article') ): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo Yii::$app->session->getFlash('article'); ?>
        </div>
    <?php endif;?>

    <h1><?= Html::encode($this->title) ?></h1>


    <?php if (($user && $model->user_id == $user->id) || Yii::$app->user->can('admin')):?>
        <p>
            <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить статью', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы уверены, что хотите удалить данную статью?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php endif;?>

    <div class="row">
        Дата размещения: <?= $model->getDate()?>
    </div>

    <div class="row">

        <div class="article">

            <div class="image">
                <div class="inner-image">
                    <?= Html::img('/uploads/users/'.$model->image, ['alt' => $model->title, 'width' => '100%']) ?>
                </div>
            </div>

            <div class="description">
                <p>
                    <?= $model->description ?>
                </p>
            </div>

            <div class="row">

                <div class="row article-info">
                    <div class="col-md-4 col-sm-4">
                        <?= empty($model->position) ? 'Репосты: ' . $model->point : $model->position . ' место' ?>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <?= Html::a('Поддержать,<br>сделав репост', $vk_link, ['target'=> '_blank;']) ?>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <span>Автор</span>
                        <?= Html::a(
                            Html::img($model->author->photo, [
                                'alt' => $model->author->first_name . ' ' . $model->author->last_name,
                                'title' => $model->author->first_name . ' ' . $model->author->last_name,
                                'style'=> 'border-radius: 50%;',
                            ]),
                            'https://vk.com/id'.$model->author->id,
                            ['target'=> '_blank;']
                        ) ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
