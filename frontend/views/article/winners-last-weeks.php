<?php

use frontend\models\Article;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Победители прошлых недель';
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user->identity;
?>

<div class="article" style="background-color: rgba(205,229,227,0.04)">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $articles = $dataProvider->getModels(); ?>

    <div class="row text-center article-list">
        <?php foreach ($articles as $articleNum => $article):?>
            <div class="col-md-6">
                <?= $this->render('_article', [
                    'article' => $article,
                ]) ?>
            </div>
            <?= ($articleNum + 1) % 2 == 0 ? '<div class="clearfix"></div>' : '' ?>
        <?php endforeach;?>
    </div>

    <?= yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{pager}',
    ])?>

</div>

