<?php

use yii\grid\ActionColumn;
use yii\grid\SerialColumn;
use frontend\models\Article;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Обработка статей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
        echo $this->render('_search', ['model' => $searchModel]);

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => SerialColumn::class],

                [
                    'attribute' => 'status',
                    'filter' => Article::$article_statuses_map,
                    'value' => static function ($data) {
                        return Article::$article_statuses_map[$data->status];
                    }
                ],
                'title',
                [
                    'format' => 'html',
                    'label' => 'Описание',
                    'value' => static function ($data) {
                        /** @var Article $data */
                        return substr($data->getDescription(), 0, 80);
                    }
                ],
                [
                    'format' => 'html',
                    'label' => 'Изображение',
                    'value' => static function ($data) {
                        /** @var Article $data */
                        return Html::img($data->getImage(), ['width' => 90]);
                    }
                ],
                [
                    'format' => 'html',
                    'label' => 'Дата',
                    'value' => static function ($data) {
                        /** @var Article $data */
                        return $data->getDate();
                    }
                ],

                ['class' => ActionColumn::class],
            ],
        ]);
?>


</div>
