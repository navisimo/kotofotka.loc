<?php

use frontend\models\Article;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Обновление баллов';
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user->identity;
?>

<div class="article" style="background-color: rgba(205,229,227,0.04)">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $articles = $dataProvider->getModels(); ?>

    <div class="row">
        <?php foreach ($articles as $article):?>
            <div class="col-lg-6" style="max-height: 1200px">
                <?= $this->render('_articles', [
                    'article' => $article,
                ]) ?>
            </div>
        <?php endforeach;?>
    </div>

    <?= yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{pager}',
    ])?>

</div>

