<?php

use yii\helpers\Html;
/** @var \frontend\models\Article $article */
?>

<div class=" row article-form">
        <h2><?= $article->title?></h2>
    <div class="image">
        <div class="inner-image">
            <a href='/article/view?id=<?=$article->id?>'>
                <?= Html::img('/uploads/users/'.$article->image, ['alt' => $article->title, 'width' => '100%']) ?>
            </a>
        </div>
    </div>

    <div class="description">
        <p>
            <?php
            $string = $article->description;
            if(strlen($string) > 230){
                $string = substr($string, 0, 230);
                $string = rtrim($string, '!,.-');
                $string = substr($string, 0, strrpos($string, ' '));
            }
            echo $string;

            if(strlen($article->description) > 230):?>
                <a href='/article/view?id=<?=$article->id?>'>...</a>
            <?php endif;?>
        </p>
    </div>
<hr>
    <div class="blocks">
        Баллы: <?= $article->point?>
    </div>
<hr>
    <?php if (!empty($article->position)):?>
    Место: <?= $article->position ?>
        <hr>
    <?php endif;?>

    <div class="row">
        <div class="col-lg-2 col-xs-4">
            <div class="blocks">
                <a class="btn btn-default" href="https://vk.com/club<?= \Yii::$app->params['groupID']?>?w=wall-<?= \Yii::$app->params['groupID'] . '_' . $article->publication_id ?>">Поддержи участника</a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-12">
            <div class="blocks">
                <?= Html::img($article->author->photo, ['alt' => $article->author->first_name . ' ' . $article->author->last_name, 'style'=> 'border-radius: 50%;']) ?>
                <?= $article->author->first_name . ' ' . $article->author->last_name?>
            </div>
        </div>

        <div class="col-lg-3 col-xs-12">
            <div class="blocks">
                Дата: <?= $article->getDate()?>
            </div>
        </div>
    </div>

</div>
