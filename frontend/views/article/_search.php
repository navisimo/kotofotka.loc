<?php

use frontend\models\Article;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\ArticleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-search row">

    <?php $form = ActiveForm::begin([
        'action' => ['status'],
        'method' => 'get',
    ]); ?>
    <div class="col-lg-6">
        <?= $form->field($model, 'id') ?>
        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'user_id') ?>

    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'date') ?>
        <?= $form->field($model, 'status')->dropDownList(
            Article::$article_statuses_map
        ) ?>
        <div class="form-group">
            <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Сбросить', ['class' => 'btn btn-outline-secondary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
