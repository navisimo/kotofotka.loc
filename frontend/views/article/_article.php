<?php

use yii\helpers\Html;
/** @var \frontend\models\Article $article */

$link = '/article/view?id='.$article->id;
$vk_link = 'https://vk.com/club'
    . \Yii::$app->params['groupID']
    . '?w=wall-' . \Yii::$app->params['groupID']
    . '_'
    . $article->publication_id;

?>

<div class="article_wrap">
    <h2><?= Html::a($article->title, $link) ?></h2>
    <a class="inner-image" href='<?= $link ?>'>
        <?= Html::img('/uploads/users/'.$article->image, ['alt' => $article->title]) ?>
    </a>
    <div class="description">
        <p>
            <?php
            $string = $article->description;
            if(strlen($string) > 230){
                $string = substr($string, 0, 230);
                $string = rtrim($string, '!,.-');
                $string = substr($string, 0, strrpos($string, ' '));
            }
            echo $string;

            if(strlen($article->description) > 230):?>
                <?= Html::a('... читать далее &raquo;', $link) ?>
            <?php endif;?>
        </p>
    </div>
    <div class="row article-info">
        <div class="col-md-4 col-sm-4">
            <?= empty($article->position) ? 'Репосты: ' . $article->point : $article->position . ' место' ?>
        </div>
        <div class="col-md-4 col-sm-4">
            <?= Html::a('Поддержать,<br>сделав репост', $vk_link, ['target'=> '_blank;']) ?>
        </div>
        <div class="col-md-4 col-sm-4">
            <span>Автор</span>
            <?= Html::a(
                    Html::img($article->author->photo, [
                        'alt' => $article->author->first_name . ' ' . $article->author->last_name,
                        'title' => $article->author->first_name . ' ' . $article->author->last_name,
                        'style'=> 'border-radius: 50%;',
                    ]),
                'https://vk.com/id'.$article->author->id,
                ['target'=> '_blank;']
            ) ?>
        </div>
    </div>

</div>
