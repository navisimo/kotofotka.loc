<script type="text/javascript" src="https://vk.com/js/api/share.js?95" charset="windows-1251"></script>
<?php
use yii\data\Pagination;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Конкурс фотографий домашних питомцев. Денежные призы!';
$user = Yii::$app->user->identity;
$appSite = \Yii::$app->params['appSite'];
?>

<div class="row marketing info_block">

    <div class="col-lg-10" >
        <div class="row">
            <div class="col-lg-7" >
                <h1 class="text-left">Конкурс фотографий с денежными призами</h1>
                <p class="text-left lead" >Каждую неделю подводится итог голосования, в котором именно Ты решишь, чей питомец станет победителем.<br></p>
                <div class="info_money">
                    <p class="text-center">Будь в тройке лидеров и получай призы:</p>
                    <dl class="dl-horizontal">
                        <dt>1 место &#128571;</dt>
                        <dd>1000 рублей</dd>
                        <dt>2 место &#128568;</dt>
                        <dd>500 рублей</dd>
                        <dt>3 место &#128573;</dt>
                        <dd>200 рублей</dd>
                    </dl>
                </div>
            </div>

            <div class="col-lg-5" >
                <img class="img-responsive" src="<?= Url::toRoute(['/uploads/kotic_ava2.png'])?>" alt="avatar_kotofotka">
            </div>
        </div>

        <blockquote class="blockquote-reverse">
            <p class="text-right">Чей четвероногий друг, по-твоему мнению, достоин первого места?<br>
                А, может, именно <strong>твой</strong> покорит сердца других любителей на этой неделе.
                <br>
                Хочешь это проверить?&#128521
                <br>
                Не забудь ознакомиться с <a href="<?= Url::toRoute(['rules'])?>">правилами конкурса</a>.
            </p>
        </blockquote>
    </div>

    <div class="col-lg-2 info_text">
        <?php if(!Yii::$app->user->isGuest):?>
            <p><a class="btn btn-primary" href="<?= Url::toRoute(['/article/create'])?>">Участвовать<br>в конкурсе</a></p>
        <?php else:?>
<!--            <a class="btn btn-primary" href="https://vk.com/app--><?//= \Yii::$app->params['appID'] . '#' . \frontend\models\Article::ACTION_CREATE_HASH ?><!--">Участвовать<br>в конкурсе</a>-->
            <button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Участвовать<br>в конкурсе</button>

            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Авторизуйтесь и создайте свой первый пост</h4>
                        </div>

                        <script type="text/javascript" src="https://vk.com/js/api/openapi.js?160"></script>
                        <script type="text/javascript">
                            VK.init({apiId: <?= $appSite?>});
                        </script>

                        <!-- VK Widget -->
                        <br>
                        <div id="vk_auth" class="center-block"></div>
                        <br>
                        <script type="text/javascript">
                            VK.Widgets.Auth("vk_auth", {"authUrl":"/user/login-vk-site"});
                        </script>
                    </div>
                </div>
            </div>


        <?php endif;?>
    </div>

</div>

<?php $model = $dataProvider->getModels(); ?>
<div class="row text-center article-list">

    <?php foreach ($model as $articleNum => $article):?>
        <div class="col-md-6">
            <?= $this->render('@frontend/views/article/_article', [
                'article' => $article,
            ]) ?>
        </div>
        <?= ($articleNum + 1) % 2 == 0 ? '<div class="clearfix"></div>' : '' ?>
    <?php endforeach;?>

</div>
<?= yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{pager}',
])?>


