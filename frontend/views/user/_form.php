<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-4">
            <?php if(Yii::$app->user->can('admin')) :?>
                <?= $form->field($model, 'id')->textInput() ?>
            <?php endif ;?>
            <?= Html::img('/uploads/users/'.$model->photo, ['alt' => $model->first_name.' '.$model->last_name, 'style'=> 'max-width: 100px']) ?>
        <br><br>
            <?= $form->field($model, 'pay_metod')->radioList(
                $options = [
                    'pay_phone'=>'Оплата на счет номера телефона',
                    'pay_yandex'=>'Оплата на счет Яндекс.Деньги',
                    'pay_qiwi'=>'Оплата на счет Киви.Кошелек',
                    'pay_vk'=>'Оплата на счет Вк',
                ]) ?>
        </div>


        <div class="col-lg-8">
            <?= $form->field($model, 'pay_phone')->textInput() ?>
            <?= $form->field($model, 'pay_yandex')->textInput() ?>
            <?= $form->field($model, 'pay_qiwi')->textInput() ?>
        </div>

    </div>
    <div class="form-group">
        <br>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end(); ?>

</div>
