<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title ='Профиль: '.$model->first_name .' '. $model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить данные', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить профиль', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы, действительно, хотите удалить свой профиль?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'first_name',
            'last_name',
            [
                'format' => 'html',
                'label' => 'Изображение пользователя',
                'value' => function($data){
//        var_dump($data->getImage());
                    return Html::img($data->getImage(),['width' => 50]);
                }
            ],
            'email:email',
            'pay_metod',
            'pay_phone',
            'pay_yandex',
            'pay_qiwi',
        ],
    ]) ?>
</div>
