<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\widgets\Menu;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <!--    обеспечение правильной визуализации и сенсорного масштабирования-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Навигация</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                <a href="<?= Url::toRoute(['/site/index'])?>" class="navbar-brand text-uppercase">kotofotka</a>
            </div>

            <?php if (!Yii::$app->user->isGuest):?>
                <ul class="nav navbar-nav navbar-right collapse navbar-collapse">
                    <?php if(Yii::$app->user->can('admin')):?>
                        <li><a href="<?= Url::toRoute(['/user/index'])?>">Просмотр всех пользователей</a></li>
                    <?php endif;?>
                    <?php if(Yii::$app->user->can('moder')):?>
                        <li><a href="<?= Url::toRoute(['/article/status'])?>">Обработка статей</a></li>
                    <?php endif;?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Публикации <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= Url::toRoute(['/article/new-publications'])?>">Новые публикации</a></li>
                            <li><a href="<?= Url::toRoute(['/article/the-best-publications'])?>">Лучшие публикации</a></li>
                            <li class="divider"></li>
                            <li><a href="<?= Url::toRoute(['/article/winners-last-weeks'])?>">Победители прошлых недель</a></li>
                        </ul>
                    </li>
                    <li><a href="<?= Url::toRoute(['/article?id='.Yii::$app->user->id])?>">Мои публикации</a></li>
                    <li><a href="<?= Url::toRoute(['/user/update?id='.Yii::$app->user->id])?>">Мой профиль</a></li>
                    <li><a href="<?= Url::toRoute(['/user/logout'])?>">Выход (<?=Yii::$app->user->identity->first_name?>)</a></li>
                </ul>
            <?php else: ?>
                <ul class="nav navbar-nav navbar-right collapse navbar-collapse">
                            <li><a href="<?= Url::toRoute(['/article/new-publications'])?>">Новые публикации</a></li>
                            <li><a href="<?= Url::toRoute(['/article/the-best-publications'])?>">Лучшие публикации</a></li>
                            <li class="divider"></li>
                            <li><a href="<?= Url::toRoute(['/article/winners-last-weeks'])?>">Победители прошлых недель</a></li>
                </ul>
            <?php endif;?>
        </div>
    </div>

    <!--            body { background: url(/images/138.jpg) 0 0 repeat; }-->

    <div class="container main_container">
        <?= $content ?>
    </div>


    <footer class="footer">
        <div class="container-fluid">
            <p class="pull-left">&copy; Все права защищены <?= date('Y') ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>

    </body>
    </html>
<?php $this->endPage() ?>