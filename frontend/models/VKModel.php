<?php
namespace frontend\models;

use VK\Client\VKApiClient;
use Yii;
use yii\helpers\VarDumper;

class VKModel
{
    /**
     * @var VKApiClient
     */
    private $client;

    /**
     * @var string
     */
    private $access_token;

    /**
     * @var string
     */
    private $groupID;


    public function __construct()
    {
        $this->client = new VKApiClient();
        $this->access_token = \Yii::$app->params['accessToken'];
        $this->groupID = \Yii::$app->params['groupID'];
    }

    public function publicationArticle (Article $article)
    {
        $pathImage = Yii::getAlias('@frontend') . '/web/uploads/users/';

        try {
            $address = $this->client->photos()->getWallUploadServer($this->access_token);
            $photo = $this->client->getRequest()->upload($address['upload_url'], 'photo', $pathImage . $article->image);

            $response_save_photo = $this->client->photos()->saveWallPhoto($this->access_token, array(
                'server' => $photo['server'],
                'photo' => $photo['photo'],
                'hash' => $photo['hash'],
            ));


            $response = $this->client->wall()->post($this->access_token, array(
                'owner_id' => '-' . $this->groupID,
                //'friends_only' => 1,
                'from_group' => 1,
                'message' => $article->title . PHP_EOL . PHP_EOL . $article->description . PHP_EOL . PHP_EOL . 'Автор: https://vk.com/id' . $article->author->id,
                'mute_notifications' => 1,
                'attachments' => 'photo' . $response_save_photo[0]['owner_id'] . '_' . $response_save_photo[0]['id'],
            ));

            //var_dump('photo'.'-' . $groupID .'_'. $response_save_photo[0]['id'], $response_save_photo);die;

        } catch (\Exception $exception) {
//            VarDumper::dump($exception);die;
            return null;
        }

        return $response['post_id'];
    }

    public function getUserData($id)
    {
        $response = $this->client->users()->get($this->access_token, array(
            'user_id' => $id,
            'fields' => 'photo_50',
        ));

        return $response[0];
    }

    public function getRepostCount($id)
    {

        $repost = $this->client->wall()->getReposts($this->access_token, array(
            'owner_id' => '-' . $this->groupID,
            'post_id' => $id,
        ));

        return count($repost['profiles']);
    }

}