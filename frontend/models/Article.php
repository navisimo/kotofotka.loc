<?php

namespace frontend\models;

use common\models\User;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $date
 * @property int $point
 * @property int $position
 * @property int $user_id
 * @property int $status
 * @property int $publication_id
 * @property  $lastPointUpdate
 * @property User $author
 */
class Article extends \yii\db\ActiveRecord
{
    public $imagesPath;

    const SCENARIO_CREATE = 'create';

    const ACTION_CREATE_HASH = 'create';

    const ARTICLE_PROCESSING_STATUS = 1; // в обработке
    const ARTICLE_MODIFY_STATUS  = 2;    // отправлен пользователю (отправить сообщение пользователю)
    const ARTICLE_RETREATMENT_STATUS = 3;// повторная обработка
    const ARTICLE_DISALLOW_STATUS = 5;  // отказ в статье( отправить сообщение пользователю)
    const ARTICLE_ALLOW_STATUS = 10;      // статья одобрена

    public static $article_statuses_map = [
        self::ARTICLE_PROCESSING_STATUS => 'в ожидании обработки модератором',
        self::ARTICLE_MODIFY_STATUS => 'корректировка пользователем',
        self::ARTICLE_RETREATMENT_STATUS => 'повторная обработка модератором',
        self::ARTICLE_DISALLOW_STATUS => 'отказ в статье',
        self::ARTICLE_ALLOW_STATUS => 'статья одобрена',
    ];


    public static function tableName()
    {
        return 'article';
    }

    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            ['image', 'required', 'on' => self::SCENARIO_CREATE],
            [['title'], 'string', 'length' => [10, 100]],
            [['description'],  'string', 'length' => [200, 1000]],
            [['title', 'description'], 'filter','filter'=>'strip_tags'],
            ['title','filter','filter'=>function($value){
                return mb_strtoupper(mb_substr($value, 0, 1)) . mb_substr($value, 1);
            }],
            ['description','filter','filter'=>function($value){
                return mb_strtoupper(mb_substr($value, 0, 1)) . mb_substr($value, 1);
            }],
            [['date'], 'date', 'format'=> 'php:Y-m-d'],
            [['date'], 'default', 'value'=> date('Y-m-d')],
            [['point', 'user_id', 'status'], 'integer'],
            ['image', 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif'],
//				'minWidth' => 1000, 'maxWidth' => 2000,
//				'minHeight' => 1000, 'maxHeight' =>2000,

            [['point'], 'default', 'value' => 0],

        ];
    }


        /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'title' => 'Заголовок',
            'description' => 'Ваша история',
            'date' => 'Дата создания',
            'point' => 'Репосты',
            'user_id' => '# пользователя',
            'status' => 'Статус',
            'image' => 'Изображение',
            'position' => 'Позиция',
            'publication_id' => 'ID публикации Вк',
            'lastPointUpdate' => 'Последнее обновление баллов',
        ];
    }

    /**
     * @param $id_user
     * @param $id_article
     * @throws \yii\base\Exception
     */
    public function upload ($file, $id_user, $currentImage)
    {
        $pathImage = Yii::getAlias('@frontend') . '/web/uploads/users/';

        if(!empty($currentImage) && $currentImage !== null)
        {
            if(file_exists($pathImage . '' . $currentImage))
            {
                unlink($pathImage . '' . $currentImage);
            }
        }


        FileHelper::createDirectory($pathImage . $id_user . '/articles/');
        $this->image = $id_user . '/articles/' . time( ) . '_' . mt_rand(100000, 1000000) . '.jpg';

        $file->saveAs($pathImage . '' . $this->image);

            return $this->image;
    }

    public function getImage()
    {
        return $this->image ? '/uploads/users/'.$this->image : '/uploads/no-image.png';
    }

    public function getDescription()
    {
        $string = $this->description;
        if(strlen($string) > 80){
            $string = substr($string, 0, 80);
            $string = rtrim($string, "!,.-");
            $string = substr($string, 0, strrpos($string, ' ')) . '...';
        }
        return $string;

    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getDate()
    {
        return Yii::$app->formatter->asDate($this->date, 'php:d F Y');
    }

    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id'=>'user_id']);
    }


    public function beforeSave($insert)
    {
        if ((int)$this->status === self::ARTICLE_ALLOW_STATUS && empty($this->publication_id)) {

            $VKModel = new VKModel();
            $this->publication_id = $VKModel->publicationArticle($this);

            if (!$this->publication_id) {
                Yii::$app->session->setFlash('article', 'Внимание! Пост не опубликован в группе. Произошла ошибка. Обратитесь к администратору сайта.');
            }
        }

        return parent::beforeSave($insert);
    }

}
