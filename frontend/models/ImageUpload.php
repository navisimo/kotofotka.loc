<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;

class ImageUpload extends Model
{
    public $image;

    public function rules()
    {
        return [
            [['image'], 'required'],
            [['image'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    public function uploadFile($file, $currentImage)
    {
        $this->image = $file;
        $imageName = time( ).'_'.mt_rand(100000, 1000000).'.jpg';
        $id = Yii::$app->user->id;
        $pathImage = Yii::getAlias('@frontend').'/web/uploads/users/';
        if($this->validate())
        {
            if (!empty($currentImage) && $currentImage != null)
            {
                if(file_exists($pathImage.$currentImage))//картинка существует на сервере
                {
                    unlink($pathImage.$currentImage);
                }
            }

            FileHelper::createDirectory($pathImage);

            $file->saveAs($pathImage.$id.'/'.$imageName);

            return $id.'/'.$imageName;
        }
        return false;
    }

    public function saveToDirectory($uid, $photo, $currentImage)
    {
        $pathImage = Yii::getAlias('@frontend').'/web/uploads/users/';

        if (!empty($currentImage) && $currentImage !== null && file_exists($pathImage . $currentImage)) {
            unlink($pathImage.$currentImage);
        }

        $imageName = 'ava_' . time( ) . '_' . mt_rand(100000, 1000000) . '.jpg';// или strtolower(md5(uniqid($filename)).'.jpg')
        $pathImage .= $uid;


        FileHelper::createDirectory($pathImage); //существует ли папка, если нет - создать

        copy($photo, $pathImage . '/' . $imageName);//сохраняем файл в дерикторию users->$uid

        return $uid.'/'.$imageName;
    }


}