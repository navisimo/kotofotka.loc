<?php
namespace frontend\controllers;

use frontend\models\Article;
use frontend\models\VKModel;
use Yii;
use common\models\User;
use common\models\UserSearch;
use frontend\models\ImageUpload;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            //Доступ только для админа
            [
                'class' => AccessControl::className(),
                'only' => ['index', 'create'],
                'rules' => [
                    [
                        'actions' => ['index', 'create'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],

            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $searchModel = new UserSearch();
        //queryParams - В запросе GET значения параметров.
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \Exception
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('admin')) {

            $model = new User();

            $model->generateAuthKey();

            if ($model->load(Yii::$app->request->post()) && $model->save())
            {
                $auth = Yii::$app->authManager;
                $role = $auth->getRole('user');
                $auth->assign($role, $model->id);

                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        }

        return $this->redirect(['site/index']);
    }

    /**Обновляет существующую модель пользователя.
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model !== null) {
            $currentImage = $model->photo;
            if ($model->load(Yii::$app->request->post())) {
                if (Yii::$app->request->isPost) {
                    // получаем экземпляр загруженного файла
                    $file = UploadedFile::getInstance($model, 'photo');
                    $model->photo = $model->uploadFile($file, $currentImage);

                    $model->save();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /*Удаляет существующую модель пользователя - Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $user = $this->findModel($id);
        if($user) {
            $user->first_name = 'DELETE';
            $user->last_name = 'DELETE';

            $user->save(false);
            $this->actionLogout();
        }
        return $this->redirect(['index']);
    }


    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * @param $uid
     * @param $first_name
     * @param $last_name
     * @param $photo
     * @param $hash
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionLoginVk()
    {
        //VarDumper::dump(Yii::$app->request->get('first_name'));die;
        $sign = '';

        foreach (Yii::$app->request->get() as $key => $param) {

            if ($key == 'hash' || $key == 'sign') {
                continue;
            }

            $sign .=$param;
        }

        $secret = \Yii::$app->params['secretKeyApp'];

        if(hash_hmac('sha256', $sign, $secret) == Yii::$app->request->get('sign'))
        {
            $vk = new VKModel();
            $vkUserData = $vk->getUserData(Yii::$app->request->get('viewer_id'));

            $user = User::findOne(Yii::$app->request->get('viewer_id'));

            if(!$user) {
                $user = new User();
                $user->id = $vkUserData['id'];
                $user->generateAuthKey();

                if ($user->save(false))
                {
                    $auth = Yii::$app->authManager;
                    $role = $auth->getRole('user');
                    $auth->assign($role, $user->id);
                }
            }

            $user->first_name = $vkUserData['first_name'];
            $user->last_name = $vkUserData['last_name'];
            $user->photo = $vkUserData['photo_50'];

            $user->save();
            Yii::$app->user->login($user);

            if(Yii::$app->request->get('hash') === Article::ACTION_CREATE_HASH) {
                return $this->redirect(['article/create']);
            } else {
                return $this->redirect(['site/index']);
            }

        } else {
            throw new HttpException(401 ,'Ошибка авторизации');
        }

    }

    public function actionLoginVkSite($uid, $first_name, $last_name, $photo, $hash)
    {
        $secret = \Yii::$app->params['secretKeyAppSite'];
        $app = \Yii::$app->params['appSite'];

        $user_hash = md5($app.$uid.$secret);
        if($user_hash !== $hash) {
            throw new HttpException(401 ,'Ошибка авторизации');
        }

        $user = $this->updateUserFromVK($uid);
        Yii::$app->user->login($user);

        return $this->redirect(['article/create']);
    }

    protected function updateUserFromVK($userId)
    {
        $vk = new VKModel();
        $vkUserData = $vk->getUserData($userId);
        $user = User::findOne($userId);

        if(!$user) {
            $user = new User();
            $user->id = $vkUserData['id'];
            $user->generateAuthKey();

            if ($user->save(false))
            {
                $auth = Yii::$app->authManager;
                $role = $auth->getRole('user');
                $auth->assign($role, $user->id);
            }
        }

        $user->first_name = $vkUserData['first_name'];
        $user->last_name = $vkUserData['last_name'];
        $user->photo = $vkUserData['photo_50'];

        $user->save();

        return $user;
    }

    /**
     * @param $id
     * @return User|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}