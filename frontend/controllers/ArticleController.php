<?php

namespace frontend\controllers;

use frontend\models\VKModel;
use Yii;
use frontend\models\Article;
use frontend\models\ArticleSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\VarDumper;
//echo '<pre>';
//var_dump(Yii::$app->request->post());
//echo '</pre>';
//die();


/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'status', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['status'],
                        'roles' => ['moder'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionStatus()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->searchStatus(Yii::$app->request->queryParams);

        return $this->render('status', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article(['scenario' => Article::SCENARIO_CREATE]);
        $model->status = Article::ARTICLE_PROCESSING_STATUS;
        $model->user_id = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()))
        {
            if (Yii::$app->request->isPost)
            {
                $file = UploadedFile::getInstance($model, 'image');

                $model->image = $model->upload($file,Yii::$app->user->id, $model->image);

                $model->save();

            }
            Yii::$app->session->setFlash('article', 'Статья будет доступна для публикации после обработки модератором');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->status == Article::ARTICLE_ALLOW_STATUS && !Yii::$app->user->can('moder')) {
            throw new HttpException(403 ,'Материал уже опубликован. Для измениения обратитесь к администратору сайта\группы');
        }

        $currentImage = $model->image;
        if ($model->load(Yii::$app->request->post()))
        {
            if (Yii::$app->request->isPost)
            {

                $file = UploadedFile::getInstance($model, 'image');

                if ($file) {
                    $model->image = $model->upload($file,Yii::$app->user->id, $currentImage);
                }

            }
            $model->image = $currentImage;
            $model->save();

            Yii::$app->session->setFlash('article', 'Статья будет доступна для публикации после обработки модератором');
            return $this->redirect(['view', 'id' => $model->id]);
        }


        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->status == Article::ARTICLE_ALLOW_STATUS && !Yii::$app->user->can('moder')) {
            throw new HttpException(403 ,'Материал уже опубликован. Для удаления обратитесь к администратору сайта\группы');
        }

        unlink(Yii::getAlias('@frontend') . '/web/uploads/users/' . '' . $model->image);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionNewPublications()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->searchNew(Yii::$app->request->queryParams);

        return $this->render('new-publications', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTheBestPublications()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->searchBest(Yii::$app->request->queryParams);

        return $this->render('the-best-publications', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionWinnersLastWeeks()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->searchPosition(Yii::$app->request->queryParams);

        return $this->render('winners-last-weeks', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

    public function actionUpdatePoint() {
        //получить все articles с датой публикации - меньше месяца назад
        //SELECT * FROM TABLE WHERE tc_date >= DATE_SUB(CURRENT_DATE, INTERVAL 30 DAY)

        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->searchRepost(Yii::$app->request->queryParams);

        $articles = $dataProvider->getModels();
        $vk = new VKModel();


        foreach ($articles as $article) {
            $article->point = $vk->getRepostCount($article->id);
            $article->save();
            sleep(1);
        }

        die();

        return $this->render('update-point', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);

    }
}
